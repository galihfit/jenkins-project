terraform {
  backend "s3" {
    bucket = "s3-ganis-bucket"
    key    = "aws-tf-workshop/ec2-fuad"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

variable "ami" {
  type    = string
  default = "ami-03ca998611da0fe12"
}

variable "instance_type" {
  type    = string
  default = "t3.micro"
}

variable "tags" {
  type = map(string)
  default = {
    Name      = "ec2-fuad"
    ManagedBy = "Terrafrom"
  }
}

variable "subnet_id" {
  type    = string
  default = "subnet-0fb37140a2fbaec9c"
}

variable "security_groups_id" {
  type = list(string)
  default = [
    "sg-09add3249e9626859"
  ]
}

resource "aws_instance" "default" {
  ami           = var.ami
  instance_type = var.instance_type

  subnet_id       = var.subnet_id
  vpc_security_group_ids = var.security_groups_id

  user_data = <<EOF
    #!/bin/bash
    sudo apt install nginx -y
    sudo systemctl start nginx
  EOF

  tags = var.tags
}
